package controllers

import (
	"gocmdb/base/controllers/base"
)

type HomeController struct {
	base.BaseController
}

func (c *HomeController) Index() {
	c.TplName = "home/index.html"
}
