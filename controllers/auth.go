package controllers

import (
	"fmt"
	"net/http"

	"gocmdb/base/controllers/base"
	"gocmdb/forms"
	"gocmdb/services"

	"github.com/astaxie/beego"
)

type AuthController struct {
	base.BaseController
}

func (c *AuthController) Login() {
	// fmt.Println(c.GetControllerAndAction())
	form := &forms.LoginForm{}
	if c.Ctx.Input.IsPost() {
		if err := c.ParseForm(form); err == nil {
			user := services.UserService.GetByName(form.Name)
			if user == nil {
				beego.Error(fmt.Sprintf("%s 用户不存在", form.Name))
			} else if user.ValidPassword(form.Password) {
				c.Redirect("/home/index", http.StatusFound)
			} else {
				beego.Error(fmt.Sprintf("用户密码错误: %s", form.Name))
			}
		} else {
			beego.Error(fmt.Sprintf("用户或密码错误: %s", form.Name))
		}
	}
	c.Data["form"] = form
	c.TplName = "auth/login.html"
}
