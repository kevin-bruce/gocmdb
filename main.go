package main

import (
	"fmt"
	_ "gocmdb/controllers"
	"log"

	_ "gocmdb/routers"

	_ "gocmdb/models"

	_ "github.com/go-sql-driver/mysql"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func main() {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&loc=PRC&parseTime=true",
		beego.AppConfig.DefaultString("mysql::User", "kevin"),
		beego.AppConfig.DefaultString("mysql::Password", "kevin"),
		beego.AppConfig.DefaultString("mysql::Host", "10.0.1.3"),
		beego.AppConfig.DefaultString("mysql::Port", "3306"),
		beego.AppConfig.DefaultString("mysql::DBName", "gocmdb"),
	)
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", dsn)
	orm.RunCommand()

	if db, err := orm.GetDB("default"); err != nil {
		log.Fatal(err)
	} else if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	beego.Run()
}
